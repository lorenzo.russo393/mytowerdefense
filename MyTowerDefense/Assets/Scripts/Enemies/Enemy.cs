using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using static UnityEngine.GraphicsBuffer;

public class Enemy : MonoBehaviour
{
    protected BasicTurret bt;
    protected RifleTurret rt;
    protected AreaTurret at;

    protected float maxHP;
    public float currentHP;
    public int damage;
    public Transform[] leftTargets;
    public Transform[] rightTargets;
    protected NavMeshAgent agent;

    public int indexLeft;
    public int indexRight;

    public Image healthbar;
    [SerializeField] protected GameObject bar;
    [SerializeField] Canvas canvas;

    [SerializeField] protected GameObject impactEffect;
    protected bool speedUpdated = false;

    protected float speed;

    protected void WhenTakeDamage()
    {
        if(currentHP != maxHP)
        {
            bar.SetActive(true);
        }
    }

    public void HealthBar()
    {
        canvas.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);
    }
}
