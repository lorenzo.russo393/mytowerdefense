using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StandardEnemy : Enemy
{
    EnemySpawn es;
    GameManager gm;

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
        es = FindObjectOfType<EnemySpawn>();

        maxHP = 75;
        currentHP = maxHP;
        agent = GetComponent<NavMeshAgent>();
        speed = agent.speed;
    }

    private void Update()
    {
        if(gm.escActive)
        {
            agent.speed = 0;
        }
        else
        {
            agent.speed = speed;
        }

        Movement();

        WhenTakeDamage();

        if (currentHP <= 0)
        {
            EnemySpawn es = FindObjectOfType<EnemySpawn>();
            es.enemyDead++;

            Destroy(gameObject);
            GameObject effect = Instantiate(impactEffect, transform.position, Quaternion.identity);
            Destroy(effect, 1);
        }

        HealthBar();

        healthbar.fillAmount = currentHP / 75;

        LevelWaves lw = FindObjectOfType<LevelWaves>();

        if (lw.roundCount >= 10 && lw.roundCount < 20 && !speedUpdated)
        {
            agent.speed += 0.5f;
            speedUpdated = true;
        }   

        if (lw.roundCount >= 20 && !speedUpdated)
        {
            agent.speed += 1.5f; 
            speedUpdated = true;
        }
    }

    void Movement()
    {
        if (es.spawnLeft)
        {
            agent.SetDestination(leftTargets[indexLeft].position);

            if (Vector3.Distance(agent.destination, transform.position) < 1f)
            {              
                indexLeft++;

                if(indexLeft >= leftTargets.Length)
                {
                    agent.SetDestination(leftTargets[indexLeft-1].position);
                    return;
                }
            }
        }
        else if (es.spawnRight) 
        {   
            agent.SetDestination(rightTargets[indexRight].position);

            if (Vector3.Distance(agent.destination, transform.position) < 1f)
            {
                indexRight++;

                if (indexRight == 4)
                {
                    agent.SetDestination(rightTargets[indexRight].position);
                    return;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Tower")
        {
            currentHP = 0;
            GameObject effect = Instantiate(impactEffect, transform.position, Quaternion.identity);
            Destroy(effect, 1);
        }
        if (other.tag == "Arrow")
        {
            bt = FindObjectOfType<BasicTurret>();
            currentHP -= bt.damage;            
        }
        if (other.tag == "Bullet")
        {
            rt = FindObjectOfType<RifleTurret>();
            currentHP -= rt.damage;
            Destroy(other.gameObject);
        }      
    }
}
