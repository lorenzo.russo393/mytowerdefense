using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.VisualScripting;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    LevelWaves lw;
    GameManager gm;

    [Header("Enemies")]
    [SerializeField] GameObject standardEnemy;
    [SerializeField] GameObject fastEnemy;
    [SerializeField] GameObject tankEnemy;

    [SerializeField] Transform leftSpawner;
    [SerializeField] Transform rightSpawner;

    public bool spawnLeft = false;
    public bool spawnRight = false;

    [SerializeField] int index = 0;

    public int enemyToSpawn = 0;
    public int enemyDead = 0;
    public int totalEnemiesDead = 0;
    public bool stopSpawn = false;

    // Numero di ondate di nemici in base al round
    [SerializeField] public int waveCount;

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        if (enemyDead >= enemyToSpawn)
        {
            gm.enemiesDead += enemyDead;
            enemyDead = 0;
            spawnLeft = false;
            spawnRight = false;
            stopSpawn = false;

            if (lw.roundCount <= 10)
            {
                enemyToSpawn += 8;
            }

            gameObject.SetActive(false);
            lw.roundStarted = false;
            UpgradeSpawn us = FindObjectOfType<UpgradeSpawn>();
            us.upgradeSpawned = false;
        }
    }

    void OnEnable()
    {
        lw = FindObjectOfType<LevelWaves>();

        if (lw.roundStarted)
            StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {


        if (lw.roundCount == 1 && lw.roundStarted && !stopSpawn)
        {
            //  enemyToSpawn = 7;

            int randomSpawner = Random.Range(1, 3);

            while (index < enemyToSpawn)
            {
                if (randomSpawner == 1 && !stopSpawn)
                {
                    Instantiate(standardEnemy, leftSpawner.transform.position, Quaternion.identity);
                    index++;

                    spawnLeft = true;
                    spawnRight = false;

                    yield return new WaitForSeconds(2f);
                }
                else if (randomSpawner == 2 && !stopSpawn)
                {
                    Instantiate(standardEnemy, rightSpawner.transform.position, Quaternion.identity);
                    index++;

                    spawnRight = true;
                    spawnLeft = false;

                    yield return new WaitForSeconds(2f);
                }
            }

            //yield return new WaitForSeconds(2f);

            stopSpawn = true;
            index = 0;
        }
        if (lw.roundCount == 2 && lw.roundStarted && !stopSpawn)
        {
            int randomSpawner = Random.Range(1, 3);

            while (index < enemyToSpawn)
            {
                if (randomSpawner == 1 && !stopSpawn)
                {
                    spawnLeft = true;
                    spawnRight = false;

                    if (index >= enemyToSpawn / 2)
                    {
                        Instantiate(fastEnemy, leftSpawner.transform.position, Quaternion.identity);
                    }
                    else
                    {
                        Instantiate(standardEnemy, leftSpawner.transform.position, Quaternion.identity);
                    }

                    index++;



                    if (index >= enemyToSpawn / 2)
                    {
                        yield return new WaitForSeconds(1f);
                    }
                    else
                    {
                        yield return new WaitForSeconds(2f);
                    }
                }
                else if (randomSpawner == 2 && !stopSpawn)
                {
                    spawnRight = true;
                    spawnLeft = false;

                    if (index >= enemyToSpawn / 2)
                    {
                        Instantiate(fastEnemy, rightSpawner.transform.position, Quaternion.identity);
                    }
                    else
                    {
                        Instantiate(standardEnemy, rightSpawner.transform.position, Quaternion.identity);
                    }

                    index++;



                    if (index >= enemyToSpawn / 2)
                    {
                        yield return new WaitForSeconds(1f);
                    }
                    else
                    {
                        yield return new WaitForSeconds(2f);
                    }
                }
            }

            stopSpawn = true;
            index = 0;
            gameObject.SetActive(false);
        }
        else if (lw.roundCount == 3 && lw.roundStarted && !stopSpawn)
        {
            int randomSpawner = Random.Range(1, 3);

            while (index < enemyToSpawn)
            {
                if (randomSpawner == 1 && !stopSpawn)
                {
                    spawnLeft = true;

                    if (index <= enemyToSpawn / 3)
                    {
                        Instantiate(tankEnemy, leftSpawner.transform.position, Quaternion.identity);
                    }
                    else if (index <= enemyToSpawn / 2)
                    {
                        Instantiate(standardEnemy, leftSpawner.transform.position, Quaternion.identity);
                    }
                    else
                    {
                        Instantiate(fastEnemy, leftSpawner.transform.position, Quaternion.identity);
                    }



                    if (index <= enemyToSpawn / 3 && !stopSpawn)
                    {
                        yield return new WaitForSeconds(2f);
                    }
                    else if (index <= enemyToSpawn / 2 && !stopSpawn)
                    {
                        yield return new WaitForSeconds(2f);
                    }
                    else
                    {
                        yield return new WaitForSeconds(1);
                    }

                    index++;
                }
                else
                {
                    spawnRight = true;


                    if (index <= enemyToSpawn / 3)
                    {
                        Instantiate(tankEnemy, rightSpawner.transform.position, Quaternion.identity);
                    }
                    else if (index <= enemyToSpawn / 2)
                    {
                        Instantiate(standardEnemy, rightSpawner.transform.position, Quaternion.identity);
                    }
                    else
                    {
                        Instantiate(fastEnemy, rightSpawner.transform.position, Quaternion.identity);
                    }

                    if (index <= enemyToSpawn / 3 && !stopSpawn)
                    {
                        yield return new WaitForSeconds(2f);
                    }
                    else if (index <= enemyToSpawn / 2 && !stopSpawn)
                    {
                        yield return new WaitForSeconds(2f);
                    }
                    else
                    {
                        yield return new WaitForSeconds(1);
                    }
                    index++;
                }
            }

            stopSpawn = true;
            index = 0;
            gameObject.SetActive(false);
        }
        else if (lw.roundCount > 3 && lw.roundCount < 8 && lw.roundStarted && !stopSpawn)
        {
            int randomSpawner = Random.Range(1, 3);

            Debug.Log(randomSpawner);

            while (index < enemyToSpawn)
            {
                int randomEnemyType = Random.Range(1, 5);

                if (randomSpawner == 1 && !stopSpawn)
                {
                    spawnLeft = true;

                    if (randomEnemyType == 1)
                    {
                        Instantiate(tankEnemy, leftSpawner.transform.position, Quaternion.identity);
                    }
                    else if (randomEnemyType == 2 || randomEnemyType == 3)
                    {
                        Instantiate(fastEnemy, leftSpawner.transform.position, Quaternion.identity);
                    }
                    else
                    {
                        Instantiate(standardEnemy, leftSpawner.transform.position, Quaternion.identity);
                    }



                    if (randomEnemyType == 1)
                    {
                        yield return new WaitForSeconds(2f);
                    }
                    else if (randomEnemyType == 2)
                    {
                        yield return new WaitForSeconds(2f);
                    }
                    else
                    {
                        yield return new WaitForSeconds(1);
                    }

                    index++;
                }
                else
                {
                    spawnRight = true;

                    if (randomEnemyType == 1)
                    {
                        Instantiate(tankEnemy, rightSpawner.transform.position, Quaternion.identity);
                    }
                    else if (randomEnemyType == 2 || randomEnemyType == 3)
                    {
                        Instantiate(fastEnemy, rightSpawner.transform.position, Quaternion.identity);
                    }
                    else
                    {
                        Instantiate(standardEnemy, rightSpawner.transform.position, Quaternion.identity);
                    }



                    if (randomEnemyType == 1)
                    {
                        yield return new WaitForSeconds(2f);
                    }
                    else if (randomEnemyType == 2 || randomEnemyType == 3)
                    {
                        yield return new WaitForSeconds(1f);
                    }
                    else
                    {
                        yield return new WaitForSeconds(2);
                    }
                    index++;
                }
            }

            stopSpawn = true;
            index = 0;
            gameObject.SetActive(false);
        }
        else if (lw.roundCount >= 8 && lw.roundCount <= 20 && lw.roundStarted && !stopSpawn)
        {
            int randomSpawner = Random.Range(1, 3);

            Debug.Log(randomSpawner);

            while (index < enemyToSpawn)
            {
                int randomEnemyType = Random.Range(1, 6);

                if (randomSpawner == 1 && !stopSpawn)
                {
                    spawnLeft = true;

                    if (randomEnemyType == 1 || randomEnemyType == 3)
                    {
                        Instantiate(tankEnemy, leftSpawner.transform.position, Quaternion.identity);
                    }
                    else if (randomEnemyType == 2)
                    {
                        Instantiate(standardEnemy, leftSpawner.transform.position, Quaternion.identity);
                    }
                    else
                    {
                        Instantiate(fastEnemy, leftSpawner.transform.position, Quaternion.identity);
                    }



                    if (randomEnemyType == 1 || randomEnemyType == 3)
                    {
                        yield return new WaitForSeconds(2f);
                    }
                    else if (randomEnemyType == 2)
                    {
                        yield return new WaitForSeconds(2f);
                    }
                    else
                    {
                        yield return new WaitForSeconds(1);
                    }

                    index++;
                }
                else
                {
                    spawnRight = true;

                    if (randomEnemyType == 1 || randomEnemyType == 3)
                    {
                        Instantiate(tankEnemy, rightSpawner.transform.position, Quaternion.identity);
                    }
                    else if (randomEnemyType == 2)
                    {
                        Instantiate(standardEnemy, rightSpawner.transform.position, Quaternion.identity);
                    }
                    else
                    {
                        Instantiate(fastEnemy, rightSpawner.transform.position, Quaternion.identity);
                    }

                    if (randomEnemyType == 1 || randomEnemyType == 3)
                    {
                        yield return new WaitForSeconds(2f);
                    }
                    else if (randomEnemyType == 2)
                    {
                        yield return new WaitForSeconds(2f);
                    }
                    else
                    {
                        yield return new WaitForSeconds(1);
                    }
                    index++;
                }
            }

            stopSpawn = true;
            index = 0;
            gameObject.SetActive(false);
        }
    }

}
