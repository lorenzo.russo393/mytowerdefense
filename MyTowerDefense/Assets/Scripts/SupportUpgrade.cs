using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupportUpgrade : MonoBehaviour
{
    void Update()
    {
        if(transform.childCount > 0)
        {
            gameObject.transform.GetComponent<Collider>().enabled = false;
        }
        else
        {
            gameObject.transform.GetComponent<Collider>().enabled = true;
        }
    }
}
