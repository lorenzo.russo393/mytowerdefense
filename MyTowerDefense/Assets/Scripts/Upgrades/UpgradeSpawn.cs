using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeSpawn : MonoBehaviour
{
    LevelWaves lw;
    PlaceTurretOrUpgrades pt;

    [SerializeField] public GameObject[] turretTypes;
   
    [SerializeField] public GameObject[] upgradeTypes;

    [SerializeField] Image[] positions;

    [Header("UpgradesSlotPositions")]
    [SerializeField] Transform firstSlotUpgrade;
    [SerializeField] Transform secondSlotUpgrade;
    [SerializeField] Transform thirdSlotUpgrade;

    public bool firstSlotFilled = false;
    public bool secondSlotFilled = false;
    public bool thirdSlotFilled = false;

    [SerializeField] public bool upgradeSpawned = false;

    public bool upgradesFirstRoundFinished = false;
    public bool upgradesSecondRoundFinished = false;
    public bool upgradesThirdRoundFinished = false;
    public bool upgradesFourthRoundFinished = false;

    int randomNumber;

    private void Start()
    {
        lw = FindObjectOfType<LevelWaves>();
        pt = FindObjectOfType<PlaceTurretOrUpgrades>();
    }

    private void Update()
    {
        if (!upgradeSpawned)
        {
            if (!lw.roundStarted && lw.waitUntilStartRound > 0 && lw.waitUntilStartRound < 0.5f)
            {
                if (lw.roundCount == 0 && !firstSlotFilled)
                {
                    GameObject up1 = Instantiate(turretTypes[0], firstSlotUpgrade);
                    GameObject up2 = Instantiate(turretTypes[0], secondSlotUpgrade);

                    up1.transform.parent = firstSlotUpgrade.transform;
                    up2.transform.parent = secondSlotUpgrade.transform;

                    firstSlotFilled = true;
                    secondSlotFilled = true;
                    upgradeSpawned = true;
                }

                if (lw.roundCount == 1 && !firstSlotFilled && !secondSlotFilled)
                {
                    GameObject up1 = Instantiate(turretTypes[1], firstSlotUpgrade);
                    up1.transform.parent = firstSlotUpgrade.transform;

                    firstSlotFilled = true;
                    upgradeSpawned = true;
                }
                else if (lw.roundCount == 1 && firstSlotFilled && !secondSlotFilled)
                {
                    GameObject up1 = Instantiate(turretTypes[1], secondSlotUpgrade);
                    up1.transform.parent = secondSlotUpgrade.transform;

                    secondSlotFilled = true;
                    upgradeSpawned = true;
                }
                else if (lw.roundCount == 1 && !firstSlotFilled && secondSlotFilled)
                {
                    GameObject up1 = Instantiate(turretTypes[1], firstSlotUpgrade);
                    up1.transform.parent = firstSlotUpgrade.transform;

                    firstSlotFilled = true;
                    upgradeSpawned = true;
                }
                else if (lw.roundCount == 1 && firstSlotFilled && secondSlotFilled)
                {
                    GameObject up1 = Instantiate(turretTypes[1], thirdSlotUpgrade);
                    up1.transform.parent = thirdSlotUpgrade.transform;

                    thirdSlotFilled = true;
                    upgradeSpawned = true;
                }

                if (!firstSlotFilled && !secondSlotFilled && !thirdSlotFilled && lw.roundCount > 1 && !upgradeSpawned)
                {
                    if (Random.Range(1, 3) == 1)
                    {
                        randomNumber = Random.Range(0, 3);
                        GameObject up1 = Instantiate(turretTypes[randomNumber], firstSlotUpgrade);
                        up1.transform.parent = firstSlotUpgrade.transform;
                        firstSlotFilled = true;
                        upgradeSpawned = true;
                    }
                    else
                    {
                        randomNumber = Random.Range(0, 3);
                        GameObject up1 = Instantiate(upgradeTypes[randomNumber], firstSlotUpgrade);
                        up1.transform.parent = firstSlotUpgrade.transform;
                        firstSlotFilled = true;
                        upgradeSpawned = true;
                    }
                }
                else if (firstSlotFilled && !secondSlotFilled && !thirdSlotFilled && lw.roundCount > 1 && !upgradeSpawned)
                {
                    if (Random.Range(1, 3) == 1)
                    {
                        randomNumber = Random.Range(0, 3);
                        GameObject up1 = Instantiate(turretTypes[randomNumber], secondSlotUpgrade);
                        up1.transform.parent = secondSlotUpgrade.transform;
                        secondSlotFilled = true;
                        upgradeSpawned = true;
                    }
                    else
                    {
                        randomNumber = Random.Range(0, 4);
                        GameObject up1 = Instantiate(upgradeTypes[randomNumber], secondSlotUpgrade);
                        up1.transform.parent = secondSlotUpgrade.transform;
                        secondSlotFilled = true;
                        upgradeSpawned = true;
                    }
                    upgradesThirdRoundFinished = true;
                }
                else if (firstSlotFilled && secondSlotFilled && !thirdSlotFilled && lw.roundCount > 1 && !upgradeSpawned)
                {
                    if (Random.Range(1, 3) == 1)
                    {
                        randomNumber = Random.Range(0, 3);
                        GameObject up1 = Instantiate(turretTypes[randomNumber], thirdSlotUpgrade);
                        up1.transform.parent = thirdSlotUpgrade.transform;
                        thirdSlotFilled = true;
                        upgradeSpawned = true;
                    }
                    else
                    {
                        randomNumber = Random.Range(0, 3);
                        GameObject up1 = Instantiate(upgradeTypes[randomNumber], thirdSlotUpgrade);
                        up1.transform.parent = thirdSlotUpgrade.transform;
                        thirdSlotFilled = true;
                        upgradeSpawned = true;
                    }
                }
                else if (firstSlotFilled && !secondSlotFilled && thirdSlotFilled && lw.roundCount > 1 && !upgradeSpawned)
                {
                    if (Random.Range(1, 3) == 1)
                    {
                        randomNumber = Random.Range(0, 3);
                        GameObject up1 = Instantiate(turretTypes[randomNumber], secondSlotUpgrade);
                        up1.transform.parent = secondSlotUpgrade.transform;
                        secondSlotFilled = true;
                        upgradeSpawned = true;
                    }
                    else
                    {
                        randomNumber = Random.Range(0, 3);
                        GameObject up1 = Instantiate(upgradeTypes[randomNumber], secondSlotUpgrade);
                        up1.transform.parent = secondSlotUpgrade.transform;
                        secondSlotFilled = true;
                        upgradeSpawned = true;
                    }
                }
                else if (!firstSlotFilled && secondSlotFilled && !thirdSlotFilled && lw.roundCount > 1 && !upgradeSpawned)
                {
                    if (Random.Range(1, 3) == 1)
                    {
                        randomNumber = Random.Range(0, 3);
                        GameObject up1 = Instantiate(turretTypes[randomNumber], firstSlotUpgrade);
                        up1.transform.parent = firstSlotUpgrade.transform;
                        firstSlotFilled = true;
                        upgradeSpawned = true;
                    }
                    else
                    {
                        randomNumber = Random.Range(0, 3);
                        GameObject up1 = Instantiate(upgradeTypes[randomNumber], firstSlotUpgrade);
                        up1.transform.parent = firstSlotUpgrade.transform;
                        firstSlotFilled = true;
                        upgradeSpawned = true;
                    }
                }

                /*  if (lw.roundCount == 3 && !firstSlotFilled && !secondSlotFilled && !thirdSlotFilled && !upgradesFourthRoundFinished)
                  {
                      randomNumber = Random.Range(0, 3);
                      Instantiate(turretTypes[randomNumber], firstSlotUpgrade);

                      randomNumber = Random.Range(0, 3);
                      Instantiate(upgradeTypes[randomNumber], secondSlotUpgrade);

                      firstSlotFilled = true;
                      secondSlotFilled = true;
                      upgradesFourthRoundFinished = true;
                  }
                  else if (lw.roundCount == 3 && firstSlotFilled && !secondSlotFilled && !thirdSlotFilled && !upgradesFourthRoundFinished)
                  {
                      randomNumber = Random.Range(0, 3);
                      Instantiate(turretTypes[randomNumber], secondSlotUpgrade);

                      randomNumber = Random.Range(0, 3);
                      Instantiate(upgradeTypes[randomNumber], thirdSlotUpgrade);

                      thirdSlotFilled = true;
                      secondSlotFilled = true;
                      upgradesFourthRoundFinished = true;
                  }
                  else if (lw.roundCount == 3 && firstSlotFilled && secondSlotFilled && !thirdSlotFilled && !upgradesFourthRoundFinished)
                  {
                      randomNumber = Random.Range(0, 3);

                      Instantiate(upgradeTypes[randomNumber], thirdSlotUpgrade);
                      thirdSlotFilled = true;
                      upgradesFourthRoundFinished = true;
                  }
                  else if (lw.roundCount == 3 && !firstSlotFilled && secondSlotFilled && !thirdSlotFilled && !upgradesFourthRoundFinished)
                  {
                      randomNumber = Random.Range(0, 3);
                      Instantiate(turretTypes[randomNumber], firstSlotUpgrade);

                      randomNumber = Random.Range(0, 3);
                      Instantiate(upgradeTypes[randomNumber], thirdSlotUpgrade);

                      firstSlotFilled = true;
                      thirdSlotFilled = true;
                      upgradesFourthRoundFinished = true;
                  }

                  if (lw.roundCount == 4 && !firstSlotFilled && !secondSlotFilled && !thirdSlotFilled && !upgradesThirdRoundFinished)
                  {
                      randomNumber = Random.Range(0, 3);
                      Instantiate(turretTypes[randomNumber], firstSlotUpgrade);

                      randomNumber = Random.Range(0, 3);
                      Instantiate(upgradeTypes[randomNumber], secondSlotUpgrade);

                      firstSlotFilled = true;
                      secondSlotFilled = true;
                      upgradesThirdRoundFinished = true;
                  }
                  else if (lw.roundCount == 4 && firstSlotFilled && !secondSlotFilled && !thirdSlotFilled && !upgradesThirdRoundFinished)
                  {
                      randomNumber = Random.Range(0, 3);
                      Instantiate(turretTypes[randomNumber], secondSlotUpgrade);

                      randomNumber = Random.Range(0, 3);
                      Instantiate(upgradeTypes[randomNumber], thirdSlotUpgrade);

                      firstSlotFilled = true;
                      secondSlotFilled = true;
                      upgradesThirdRoundFinished = true;
                  }
                  else if (lw.roundCount == 4 && firstSlotFilled && secondSlotFilled && !thirdSlotFilled && !upgradesThirdRoundFinished)
                  {
                      randomNumber = Random.Range(0, 3);

                      Instantiate(upgradeTypes[randomNumber], thirdSlotUpgrade);
                      firstSlotFilled = true;
                      upgradesThirdRoundFinished = true;
                  }
                  */
            }
        }
    }

}
