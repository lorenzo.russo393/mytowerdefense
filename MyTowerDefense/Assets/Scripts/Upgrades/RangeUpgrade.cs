using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class RangeUpgrade : MonoBehaviour
{
    [SerializeField] int radius = 5;

    Collider[] basicTurret = new Collider[2];
    Collider[] rifleTurret = new Collider[2];
    Collider[] areaTurret = new Collider[2];

    [SerializeField] int index = 0;

    [SerializeField] Collider[] colliders;

    [SerializeField] LayerMask turretMask;

    private void Update()
    {
        CheckTurrets();
    }

    void CheckTurrets()
    {
        colliders = Physics.OverlapSphere(transform.position, radius, turretMask);

        if (colliders.Length > 0)
        {
            foreach (Collider collider in colliders)
            {
                if(index > colliders.Length)
                {
                    index--;
                }

                if (index < colliders.Length)
                {
                    //Debug.Log(colliders[index]);

                    if (colliders[index].tag == "BasicTurret")
                    {

                        Debug.Log(basicTurret[index]);
                        basicTurret[index] = colliders[index];

                        //Debug.Log(index);

                        BasicTurret bt = basicTurret[index].gameObject.GetComponentInParent<BasicTurret>();

                        bt.range += 2;
                        index++;
                    }
                    else if (colliders[index].tag == "RifleTurret")
                    {
                        Debug.Log(rifleTurret[index]);
                        rifleTurret[index] = colliders[index];

                        //Debug.Log(index);

                        RifleTurret rt = rifleTurret[index].gameObject.GetComponentInParent<RifleTurret>();

                        rt.range += 2;
                        index++;
                    }
                    else if (colliders[index].tag == "AreaTurret")
                    {
                        Debug.Log(areaTurret[index]);
                        areaTurret[index] = colliders[index];

                        //Debug.Log(index);

                        AreaTurret at = areaTurret[index].gameObject.GetComponentInParent<AreaTurret>();

                        at.range += 2;
                        index++;
                    }
                }
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
