using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] float moveSpeed;
    [SerializeField] float zoomSpeed;

    private void Update()
    {
        //CameraMovement();
        //CameraZoom();
    }

    void CameraMovement()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(KeyCode.S) && transform.position.z > -12)
        {
            transform.Translate(Vector3.back * moveSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * moveSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * moveSpeed * Time.deltaTime, Space.World);
        }
    }

    void CameraZoom()
    {
        if(Input.GetAxis("Mouse ScrollWheel") > 0 && transform.position.y > 8)
        {
            transform.Translate(Vector3.forward * zoomSpeed * Time.deltaTime);
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0 && transform.position.y < 14.5f)
        {
            transform.Translate(Vector3.forward * -zoomSpeed * Time.deltaTime);
        }
    }
}
