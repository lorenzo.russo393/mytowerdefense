using Newtonsoft.Json.Bson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class Bullet : MonoBehaviour
{
    RifleTurret rt;
    public int damage;
    private void Start()
    {
        damage = 15;
        rt = GetComponentInParent<RifleTurret>();
    }

    private void Update()
    {
        if(rt.target != null)
        {
            MovementBullet();
        }

        if (rt.target == null)
        {
            Destroy(gameObject);
            return;
        }
    }

    void MovementBullet()
    {
        Vector3 direction = rt.target.position - transform.position;
        float distance = Time.deltaTime * 18;

        transform.Translate(direction.normalized * distance, Space.World);
        transform.LookAt(rt.target);
    }
}
