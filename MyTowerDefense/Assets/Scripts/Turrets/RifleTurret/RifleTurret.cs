using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Unity.VisualScripting;
using UnityEngine;

public class RifleTurret : Turret
{
    [SerializeField] GameObject bullet;

    [SerializeField] Transform leftMuzzle;
    [SerializeField] Transform rightMuzzle;

    [SerializeField] bool leftMuzzleOn = true;
    [SerializeField] bool rightMuzzleOn = false;

    private void Start()
    {
        InvokeRepeating("UpdateTargets", 0, 0.5f);
        StartCoroutine(Shooting());
    }

    private void Update()
    {
        ResetRange();

        TurretRotation();
    }

    void ResetRange()
    {
        int radius = 5;

        collidersRange = Physics.OverlapSphere(transform.position, radius, colliderUpRangeMask);

        if (collidersRange.Length == 0)
        {
            range = 5;
        }

        collidersDamage = Physics.OverlapSphere(transform.position, radius, colliderUpDamageMask);

        if (collidersDamage.Length == 0)
        {
            damage = 20;
        }

        collidersFireRate = Physics.OverlapSphere(transform.position, radius, colliderUpFireRateMask);

        if (collidersFireRate.Length == 0)
        {
            fireRate = 0.5f;
        }
    }

    IEnumerator Shooting()
    {
        if (target != null)
        {
            if (leftMuzzleOn && !rightMuzzleOn)
            {
                Instantiate(bullet, leftMuzzle.transform.position, Quaternion.identity, transform);

                leftMuzzleOn = false;
                rightMuzzleOn = true;
            }
            else if (rightMuzzleOn && !leftMuzzleOn)
            {
                Instantiate(bullet, rightMuzzle.transform.position, Quaternion.identity, transform);

                leftMuzzleOn = true;
                rightMuzzleOn = false;
            }
        }
        else
        {
            leftMuzzleOn = true;
            rightMuzzleOn = false;
        }
        yield return new WaitForSeconds(fireRate);

        StartCoroutine(Shooting());
    }
    
}
