using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    BasicTurret bt;

    public Transform target;

    public GameObject arrow;

    Vector3 startPosition;

    [SerializeField] float time = 0;
        public float waitToShootFirstTime;
    private void Start()
    {
        startPosition = transform.position;

        bt = FindObjectOfType<BasicTurret>();
}

    private void Update()
    {
        if(time > 0)
        time -= bt.fireRate * Time.deltaTime;

        if(time <= 0 && target != null)
        {
            ShootArrow();
        }

        if (target == null)
        {
            gameObject.SetActive(false);
            gameObject.transform.position = startPosition;
            gameObject.SetActive(true);
            return;
        }
    }

    public void ShootArrow()
    {
        if (target == null)
        {
            gameObject.SetActive(false);
            gameObject.transform.position = startPosition;
            gameObject.SetActive(true);
            return;
        }
        else
        {
            if(waitToShootFirstTime > 0)
                waitToShootFirstTime -= 1 * Time.deltaTime;

            if (waitToShootFirstTime <= 0)
            {
                Vector3 direction = target.position - transform.position;
                float distance = Time.deltaTime * 20;

                transform.Translate(direction.normalized * distance, Space.World);
                transform.LookAt(target);
                
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "StandardEnemy" || other.tag == "FastEnemy" || other.tag == "TankEnemy")
        {
            gameObject.SetActive(false);
            gameObject.transform.position = startPosition;
            gameObject.SetActive(true);

            time = 1f;
        }
    }  
}
