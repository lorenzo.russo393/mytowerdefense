using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Unity.VisualScripting;
using UnityEngine;

public class BasicTurret : Turret
{
    Arrow ar;

    PlaceTurretOrUpgrades pt;

    private void Start()
    {
        InvokeRepeating("UpdateTargets", 0, 0.5f);
        ar = FindObjectOfType<Arrow>();
        pt = FindObjectOfType<PlaceTurretOrUpgrades>();
    }

    private void Update()
    {
        ResetRange();

        if (target == null)
        {
            ar.target = null;
            return;
        }
        else
        {
            ar.target = target;
        }

        TurretRotation();       
    }

    void ResetRange()
    {
        int radius = 5;

        collidersRange = Physics.OverlapSphere(transform.position, radius, colliderUpRangeMask);

        if (collidersRange.Length == 0)
        {
            range = 7;
        }

        collidersDamage = Physics.OverlapSphere(transform.position, radius, colliderUpDamageMask);

        if (collidersDamage.Length == 0)
        {
            damage = 20;
        }

        collidersFireRate = Physics.OverlapSphere(transform.position, radius, colliderUpFireRateMask);

        if (collidersFireRate.Length == 0)
        {
            
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, 5);
    }
}
