using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    AreaTurret at;

    [SerializeField] float explosionRadius;

    [SerializeField] Collider[] colliders;

    [SerializeField] GameObject explosion;

    private void Start()
    {
        at = GetComponentInParent<AreaTurret>();
    }

    private void Update()
    {
        if(at.target != null)
        {
            MovementMissile();
        }
        if(at.target == null)
        {
            Destroy(gameObject);
            return;
        }
    }

    void Explode()
    {
        if (explosionRadius > 0)
        {
            colliders = Physics.OverlapSphere(transform.position, explosionRadius);

            foreach (Collider collider in colliders)
            {
                if (collider.tag == "StandardEnemy")
                {
                    StandardEnemy se = collider.GetComponent<StandardEnemy>();
                    //Instantiate(explosion, se.transform.position, Quaternion.identity);
                    se.currentHP -= at.damage;
                }

                if(collider.tag == "FastEnemy")
                {
                    FastEnemy fe = collider.GetComponent<FastEnemy>();
                    //Instantiate(explosion, fe.transform.position, Quaternion.identity);
                    fe.currentHP -= at.damage;
                }
                if(collider.tag == "TankEnemy")
                {
                    TankEnemy te = collider.GetComponent<TankEnemy>();
                    //Instantiate(explosion, te.transform.position, Quaternion.identity);
                    te.currentHP -= at.damage;
                }            
            }
        }
    }

    void MovementMissile()
    {
        Vector3 direction = at.target.position - transform.position;
        float distance = Time.deltaTime * 15;

        Vector3 rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * 10).eulerAngles;
        transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);

        if(direction.magnitude <= distance)
        {
            if (explosionRadius > 0)
            {
                GameObject effect = Instantiate(explosion, at.target.transform.position, Quaternion.identity);
                Destroy(effect, 3);
                Explode();
                Destroy(gameObject);
            }

        }

        transform.Translate(direction.normalized * distance, Space.World);
        transform.LookAt(at.target);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
