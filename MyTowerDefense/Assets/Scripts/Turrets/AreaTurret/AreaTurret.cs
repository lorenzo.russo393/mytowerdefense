using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaTurret : Turret
{
    [SerializeField] GameObject missile;

    [SerializeField] Transform muzzle;

    private void Start()
    {
        InvokeRepeating("UpdateTargets", 0, 0.5f);
        StartCoroutine(Shooting());
    }

    private void Update()
    {
        ResetRange();

        TurretRotation();
    }

    void ResetRange()
    {
        int radius = 5;

        collidersRange = Physics.OverlapSphere(transform.position, radius, colliderUpRangeMask);

        if (collidersRange.Length == 0)
        {
            range = 7;
        }

        collidersDamage = Physics.OverlapSphere(transform.position, radius, colliderUpDamageMask);

        if (collidersDamage.Length == 0)
        {
            damage = 25;
        }

        collidersFireRate = Physics.OverlapSphere(transform.position, radius, colliderUpFireRateMask);

        if (collidersFireRate.Length == 0)
        {
            fireRate = 1.5f;
        }
    }

    IEnumerator Shooting()
    {
        if(target != null)
        {
            Instantiate(missile, muzzle.transform.position, Quaternion.identity, transform);
        } 
        
        yield return new WaitForSeconds(fireRate);

        StartCoroutine(Shooting());
    }
}
