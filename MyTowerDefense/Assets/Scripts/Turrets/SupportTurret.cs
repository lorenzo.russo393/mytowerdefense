using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupportTurret : MonoBehaviour
{
    [SerializeField] BoxCollider bc;

    [SerializeField] Collider firstChildBc;
    [SerializeField] Collider secondChildBc;
    [SerializeField] Collider thirdChildBc;

    [SerializeField] float time = 0;

    void Update()
    {
        CheckChilds();  
    }

    void CheckChilds()
    {
        if (transform.childCount > 1 && bc.enabled)
        {
            bc.enabled = false;

            firstChildBc = transform.GetChild(1).GetComponent<Collider>();
            firstChildBc.enabled = true;
        }
        else if (transform.childCount < 2 && !bc.enabled)
        {
            time += Time.deltaTime;

            if (time > 0.9f)
            {
                bc.enabled = true;
                time = 0;
            }
        }
        else if (transform.childCount > 2 && firstChildBc.enabled)
        {
            secondChildBc = transform.GetChild(2).GetComponent<Collider>();
            secondChildBc.enabled = true;

            time += Time.deltaTime;

            if (time > 0.9f)
            {
                firstChildBc.enabled = false;
                time = 0;
            }
        }
        else if (transform.childCount > 3 && secondChildBc.enabled)
        {
            thirdChildBc = transform.GetChild(3).GetComponent<Collider>();
            thirdChildBc.enabled = true;

            time += Time.deltaTime;

            if (time > 0.9f)
            {
                secondChildBc.enabled = false;
                time = 0;
            }
        }
        if (transform.childCount < 3 && transform.childCount > 1 && !bc.enabled)
        {
            time += Time.deltaTime;

            if (time > 0.9f)
            {
                firstChildBc.enabled = true;
                time = 0;
            }
        }
        if (transform.childCount < 4 && transform.childCount > 2 && !firstChildBc.enabled)
        {
            time += Time.deltaTime;

            if (time > 0.9f)
            {
                secondChildBc.enabled = true;
                time = 0;
            }
        }
       
    }

   
}
