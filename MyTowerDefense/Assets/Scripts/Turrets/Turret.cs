using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class Turret : MonoBehaviour
{
    public int damage = 0;
    [SerializeField] public float range;
    public float fireRate = 0;
    public float shootCountDown = 0;

    public Transform target;
    public Transform partToRotate;

    [SerializeField] protected LayerMask firstSupport;
    [SerializeField] protected LayerMask secondSupport;
    [SerializeField] protected LayerMask thirdSupport;


    [SerializeField] protected LayerMask colliderUpRangeMask;
    [SerializeField] protected LayerMask colliderUpDamageMask;
    [SerializeField] protected LayerMask colliderUpFireRateMask;

    [SerializeField] protected Collider[] collidersRange;
    [SerializeField] protected Collider[] collidersDamage;
    [SerializeField] protected Collider[] collidersFireRate;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    public void TurretRotation()
    {
        if (target != null)
        {
            Vector3 direction = target.position - transform.position;
            Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, Quaternion.LookRotation(direction), Time.deltaTime * 10).eulerAngles;
            partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        }
    }

    protected void UpdateTargets()
    {
        GameObject[] standardEnemies = GameObject.FindGameObjectsWithTag("StandardEnemy");
        GameObject[] fastEnemies = GameObject.FindGameObjectsWithTag("FastEnemy");
        GameObject[] tankEnemies = GameObject.FindGameObjectsWithTag("TankEnemy");

        float minDistance = Mathf.Infinity;
        GameObject nearEnemy = null;

        foreach (GameObject enemy in standardEnemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

            if (distanceToEnemy < minDistance)
            {
                minDistance = distanceToEnemy;
                nearEnemy = enemy;
            }
        }
        foreach (GameObject enemy in fastEnemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

            if (distanceToEnemy < minDistance)
            {
                minDistance = distanceToEnemy;
                nearEnemy = enemy;
            }
        }
        foreach (GameObject enemy in tankEnemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

            if (distanceToEnemy < minDistance)
            {
                minDistance = distanceToEnemy;
                nearEnemy = enemy;
            }
        }

        if (nearEnemy != null && minDistance < range)
        {
            target = nearEnemy.transform;
        }
        else
        {
            target = null;
        }
    }


}