using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelWaves : MonoBehaviour
{
    GameManager gm;

    [SerializeField] GameObject spawner;

    [SerializeField] public float waitUntilStartRound = 0;                 // Tempo da aspettare prima di iniziare la prossima wave
    [SerializeField] public float roundDuration;                           // Durata del round corrente
    [SerializeField] public int roundCount;                                // Numero relativo al numero della wave corrente

    [SerializeField] public bool roundStarted = false;
    [SerializeField] public bool gameFinished = false;

    [SerializeField] TextMeshProUGUI stageTextSarting;
    [SerializeField] TextMeshProUGUI stageTextCurrent;
    [SerializeField] TextMeshProUGUI secondsBeforeStart;
    [SerializeField] TextMeshProUGUI advise;

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        /* if (!gameFinished)
             PreWave();

         if (gameFinished)
         {
             Debug.Log("yeah hai vinto");
         }
        */

        StartRounds();
    }

    void StartRounds()
    {
        if (!roundStarted && !gm.escActive)
        {
            waitUntilStartRound += 1 * Time.deltaTime;
            secondsBeforeStart.text = waitUntilStartRound.ToString("0");
            int roundCount1 = roundCount + 1;
            stageTextSarting.text = "Stage " + roundCount1 + " is starting..";
            stageTextCurrent.text = "";
            advise.text = "Place here turrets/upgrades that you don't want to use for the moment.";
        }

        if (waitUntilStartRound >= 15)
        {
            secondsBeforeStart.text = "";
            roundCount++;
            roundStarted = true;
            waitUntilStartRound = 0;

            stageTextCurrent.text = "Stage " + roundCount;
            stageTextSarting.text = "";
        }

        if(gameFinished)
        {
            stageTextCurrent.text = "";
            stageTextSarting.text = "";
            secondsBeforeStart.text = "";
            advise.text = "";
        }

        if (roundStarted)
        {
            advise.text = "";

            spawner.SetActive(true);

            EnemySpawn es = FindObjectOfType<EnemySpawn>();            
        }   
    }
        /* void ExecuteWaves()
         {
             roundDuration += 1 * Time.deltaTime;

             if (roundCount != 3 && roundCount != 4)
             {
                 if (roundDuration >= 70)
                 {
                     roundStarted = false;
                     spawner.SetActive(true);
                     roundDuration = 0;

                     if (roundCount == 4)
                     {
                         gameFinished = true;
                     }
                     else
                     {
                         PreWave();
                     }
                 }
             }
             else if (roundCount == 3)
             {
                 if (roundDuration >= 115)
                 {
                     roundStarted = false;
                     spawner.SetActive(true);
                     roundDuration = 0;

                     if (roundCount == 4)
                     {
                         gameFinished = true;
                     }
                     else
                     {
                         PreWave();
                     }
                 }
             }
         }


     */

        /* void PreWave()
         {
             if (!roundStarted)
             {
                 waitUntilStartRound += 1 * Time.deltaTime;
                 int roundCount1 = roundCount + 1;
                 stageTextSarting.text = "Stage " + roundCount1 + " is starting..";
                 stageTextCurrent.text = "";
             }

             if (waitUntilStartRound >= 10)
             {
                 roundCount++;
                 roundStarted = true;
                 waitUntilStartRound = 0;

                 stageTextCurrent.text = "Stage " + roundCount;
                 stageTextSarting.text = "";
             }

             if (roundStarted)
             {
                 spawner.SetActive(true);

                 ExecuteWaves();
             }
         }
        */
    
}
