using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Threading;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    LevelWaves lw;
    EnemySpawn es;

    [SerializeField] TextMeshProUGUI speedPlay;
    [SerializeField] TextMeshProUGUI statusGame;
    [SerializeField] TextMeshProUGUI secondsUntilStart;

    [SerializeField] GameObject transparentBack;

    [SerializeField] float time = 0;
    int index = 0;

    bool gamePaused = false;

    [SerializeField] bool one = false;
    [SerializeField] bool two = false;
    [SerializeField] bool three = false;

    [SerializeField] GameObject esc;
    public bool escActive = false;

    [SerializeField] GameObject gameFinish;
    [SerializeField] TextMeshProUGUI stage;
    [SerializeField] TextMeshProUGUI enemies;
    public int enemiesDead = 0;

    private void Start()
    {
        lw = FindObjectOfType<LevelWaves>();
        es = FindObjectOfType<EnemySpawn>();
    }

    private void Update()
    {
        if(lw.gameFinished)
        {
            gameFinish.SetActive(true);
            stage.text = "Stage reached: " + lw.roundCount;
            enemies.text = "Total enemies dead: " + enemiesDead;
            Time.timeScale = 0;              
        }

        Esc();
    }

    void Esc()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && !escActive)
        {
            esc.SetActive(true);
            escActive = true;
        }
        else if(Input.GetKeyDown(KeyCode.Escape) && escActive)
        {
            esc.SetActive(false);
            escActive = false;
        }
    }

    public void CloseGame()
    {
        Application.Quit();
    }

    public void Play2x()
    {
        if(Time.timeScale == 1 && !gamePaused)
        {
            Time.timeScale = 2;
            speedPlay.text = "x2";
        }
        else if(Time.timeScale == 2 && !gamePaused)
        {
            Time.timeScale = 4;
            speedPlay.text = "x4";
        }
        else if(Time.timeScale == 4 && !gamePaused)
        {
            Time.timeScale = 6;
            speedPlay.text = "x6";
        }
        else if (Time.timeScale == 6 && !gamePaused)
        {
            Time.timeScale = 1;
            speedPlay.text = "x1";
        }
    }

    public void Pause()
    {
        statusGame.text = "PAUSED";
        transparentBack.SetActive(true);   

        gamePaused= true;
        Time.timeScale = 0;
    }

    IEnumerator CountDown()
    {
        if (Time.timeScale == 0)
        {
            time += 1.2f * Time.unscaledDeltaTime;          

            if (time >= 1 && !one)
            {
                secondsUntilStart.text = time.ToString("0");
                one = true;
                index++;

                yield return new WaitForSecondsRealtime(1f);
            }
            else if (time >= 2 && time < 3 && !two)
            {
                secondsUntilStart.text = time.ToString("0");
                two = true;
                index++;

                yield return new WaitForSecondsRealtime(1f);
            }

            else if (time >= 3 && time < 4 && !three)
            {
                secondsUntilStart.text = time.ToString("0");
                three = true;
                index++;

                yield return new WaitForSecondsRealtime(1);

            }
            else if (time >= 4)
            {
                secondsUntilStart.text = "";
                one = false;
                two = false;
                three = false;
                index++;
                Time.timeScale = 1;
                speedPlay.text = "x1";
                transparentBack.SetActive(false);
                time = 0;
                gamePaused = false;
            }

            Play();
        }
    }

    public void  Play()
    {
        statusGame.text = "";
        /*if (time > 0 && time < 1)
        {
            statusGame.text = "1";
        }
        else if (time > 1 && time < 2)
        {
            statusGame.text = "2";
        }
        else if (time > 2 && time < 3)
        {
            statusGame.text = "3";
        }
        */
        StartCoroutine(CountDown());
    }
}
