using System.Collections;
using System.Collections.Generic;
using Unity.IO.LowLevel.Unsafe;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using static UnityEngine.GraphicsBuffer;

public class PlaceTurretOrUpgrades : MonoBehaviour
{
    SupportTurret st;
    UpgradeSpawn us;
    LevelWaves lw;
    GameManager gm;

    [Header("Masks")]
    [SerializeField] LayerMask basicTurretIconMask;
    [SerializeField] LayerMask rifleTurretIconMask;
    [SerializeField] LayerMask areaTurretIconMask;
    [SerializeField] LayerMask basicTurretMask;
    [SerializeField] LayerMask rifleTurretMask;
    [SerializeField] LayerMask areaTurretMask;
    [SerializeField] LayerMask rangeUpgradeIconMask;
    [SerializeField] LayerMask damageUpgradeIconMask;
    [SerializeField] LayerMask fireRateUpgradeIconMask;
    [SerializeField] LayerMask rangeUpgradeMask;
    [SerializeField] LayerMask damageUpgradeMask;
    [SerializeField] LayerMask fireRateUpgradeMask;
    [SerializeField] LayerMask firstTurretSupportMask;
    [SerializeField] LayerMask secondTurretSupportMask;
    [SerializeField] LayerMask thirdTurretSupportMask;
    [SerializeField] LayerMask supportUpgradeMask;

    [Header("Turrets prefabs")]
    [SerializeField] GameObject basicTurret;
    [SerializeField] GameObject rifleTurret;
    [SerializeField] GameObject areaTurret;

    [Header("Upgrades prefabs")]
    [SerializeField] GameObject rangeUpgrade;
    [SerializeField] GameObject damageUpgrade;
    [SerializeField] GameObject fireRateUpgrade;

    [Header("Turrets Bools")]
    public bool basicTurretTaken = false;
    public bool rifleTurretTaken = false;
    public bool areaTurretTaken = false;
    public bool rangeUpgradeTaken = false;
    public bool damageUpgradeTaken = false;
    public bool fireRateUpgradeTaken = false;
    public bool basicTurretClicked = false;
    public bool rifleTurretClicked = false;
    public bool areaTurretClicked = false;
    public bool basicTurretIsChild = false;
    public bool rifleTurretIsChild = false;
    public bool areaTurretIsChild = false;
    public bool priorityClick = false;
    public bool superPriorityClick = false;


    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
        st = FindObjectOfType<SupportTurret>();
        us = FindObjectOfType<UpgradeSpawn>();
        lw = FindObjectOfType<LevelWaves>();
    }

    private void Update()
    {
        ClickIcons();

        SupporUpgrade();

        PlaceTurretsOnSupports();

        if (Input.GetMouseButtonDown(0) && !lw.gameFinished)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 50, basicTurretMask) && !rifleTurretTaken && !basicTurretTaken && !areaTurretTaken && !rangeUpgradeTaken && !damageUpgradeTaken && !fireRateUpgradeTaken)
            {
                Destroy(hit.collider.gameObject);
                basicTurretTaken = true;
            }
            if (Physics.Raycast(ray, out hit, 50, rifleTurretMask) && !basicTurretTaken && !rifleTurretTaken && !areaTurretTaken && !rangeUpgradeTaken && !damageUpgradeTaken && !fireRateUpgradeTaken)
            {
                Destroy(hit.collider.gameObject);
                rifleTurretTaken = true;
            }
            if(Physics.Raycast(ray, out hit, 50, areaTurretMask) && !areaTurretTaken && !basicTurretTaken && !rifleTurretTaken && !rangeUpgradeTaken && !damageUpgradeTaken && !fireRateUpgradeTaken)
            {
                Destroy(hit.collider.gameObject);
                areaTurretTaken = true;
            }
            if (Physics.Raycast(ray, out hit, 50, rangeUpgradeMask) && !areaTurretTaken && !basicTurretTaken && !rifleTurretTaken && !rangeUpgradeTaken && !damageUpgradeTaken && !fireRateUpgradeTaken)
            {
                Destroy(hit.collider.gameObject);
                rangeUpgradeTaken = true;
            }
            if (Physics.Raycast(ray, out hit, 50, damageUpgradeMask) && !areaTurretTaken && !basicTurretTaken && !rifleTurretTaken && !rangeUpgradeTaken && !damageUpgradeTaken && !fireRateUpgradeTaken)
            {
                Destroy(hit.collider.gameObject);
                damageUpgradeTaken = true;
            }
            if (Physics.Raycast(ray, out hit, 50, fireRateUpgradeMask) && !areaTurretTaken && !basicTurretTaken && !rifleTurretTaken && !rangeUpgradeTaken && !damageUpgradeTaken && !fireRateUpgradeTaken)
            {
                Destroy(hit.collider.gameObject);
                fireRateUpgradeTaken = true;
            }
        }

        StackTurret();   
    }

    void SupporUpgrade()
    {
        if (Input.GetMouseButtonDown(0) && !lw.gameFinished)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if(Physics.Raycast(ray, out hit, 100, supportUpgradeMask) && basicTurretTaken)
            {
                GameObject gg = Instantiate(us.turretTypes[0], hit.transform.position, Quaternion.identity);
                gg.transform.parent = hit.transform;
                gg.transform.eulerAngles = new Vector3(+60,0,0);
                gg.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
                basicTurretTaken = false;
            }
            if (Physics.Raycast(ray, out hit, 100, supportUpgradeMask) && rifleTurretTaken)
            {
                GameObject gg = Instantiate(us.turretTypes[1], hit.transform.position, Quaternion.identity);
                gg.transform.parent = hit.transform;
                gg.transform.eulerAngles = new Vector3(+60, 0, 0);
                gg.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
                rifleTurretTaken = false;
            }
            if (Physics.Raycast(ray, out hit, 100, supportUpgradeMask) && areaTurretTaken)
            {
                GameObject gg = Instantiate(us.turretTypes[2], hit.transform.position, Quaternion.identity);
                gg.transform.parent = hit.transform;
                gg.transform.eulerAngles = new Vector3(+60, 0, 0);
                gg.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
                areaTurretTaken = false;
            }
            if (Physics.Raycast(ray, out hit, 100, supportUpgradeMask) && rangeUpgradeTaken)
            {
                GameObject gg = Instantiate(us.upgradeTypes[2], hit.transform.position, Quaternion.identity);
                gg.transform.parent = hit.transform;
                gg.transform.eulerAngles = new Vector3(+60, 0, 0);
                gg.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
                rangeUpgradeTaken = false;
            }
            if (Physics.Raycast(ray, out hit, 100, supportUpgradeMask) && damageUpgradeTaken)
            {
                GameObject gg = Instantiate(us.upgradeTypes[0], hit.transform.position, Quaternion.identity);
                gg.transform.parent = hit.transform;
                gg.transform.eulerAngles = new Vector3(+60, 0, 0);
                gg.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
                damageUpgradeTaken = false;
            }
            if (Physics.Raycast(ray, out hit, 100, supportUpgradeMask) && fireRateUpgradeTaken)
            {
                GameObject gg = Instantiate(us.upgradeTypes[1   ], hit.transform.position, Quaternion.identity);
                gg.transform.parent = hit.transform;
                gg.transform.eulerAngles = new Vector3(+60, 0, 0);
                gg.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
                fireRateUpgradeTaken = false;
            }
        }
    }

    // Clicking icons system
    void ClickIcons()
    {
        if (Input.GetMouseButtonDown(0) && !lw.gameFinished && !gm.escActive)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100, basicTurretIconMask) && !basicTurretTaken)
            {
                Destroy(hit.collider.gameObject);

                basicTurretTaken = true;

                if (hit.collider.transform.parent.name == "Turrets/Upgrade_1")
                {
                    us.firstSlotFilled = false;
                }
                else if (hit.collider.transform.parent.name == "Turrets/Upgrade_2")
                {
                    us.secondSlotFilled = false;
                }
                else if (hit.collider.transform.parent.name == "Turrets/Upgrade_3")
                {
                    us.thirdSlotFilled = false;
                }
            }

            if (Physics.Raycast(ray, out hit, 100, rifleTurretIconMask) && !rifleTurretTaken)
            {
                Destroy(hit.collider.gameObject);

                rifleTurretTaken = true;

                if (hit.collider.transform.parent.name == "Turrets/Upgrade_1")
                {
                    us.firstSlotFilled = false;
                }
                else if (hit.collider.transform.parent.name == "Turrets/Upgrade_2")
                {
                    us.secondSlotFilled = false;
                }
                else if (hit.collider.transform.parent.name == "Turrets/Upgrade_3")
                {
                    us.thirdSlotFilled = false;
                }
            }

            if (Physics.Raycast(ray, out hit, 100, areaTurretIconMask) && !areaTurretTaken)
            {
                Destroy(hit.collider.gameObject);

                areaTurretTaken = true;

                if (hit.collider.transform.parent.name == "Turrets/Upgrade_1")
                {
                    us.firstSlotFilled = false;
                }
                else if (hit.collider.transform.parent.name == "Turrets/Upgrade_2")
                {
                    us.secondSlotFilled = false;
                }
                else if (hit.collider.transform.parent.name == "Turrets/Upgrade_3")
                {
                    us.thirdSlotFilled = false;
                }
            }

            if (Physics.Raycast(ray, out hit, 100, rangeUpgradeIconMask) && !rangeUpgradeTaken)
            {
                Destroy(hit.collider.gameObject);
                rangeUpgradeTaken = true;

                if (hit.collider.transform.parent.name == "Turrets/Upgrade_1")
                {
                    us.firstSlotFilled = false;
                }
                else if (hit.collider.transform.parent.name == "Turrets/Upgrade_2")
                {
                    us.secondSlotFilled = false;
                }
                else if (hit.collider.transform.parent.name == "Turrets/Upgrade_3")
                {
                    us.thirdSlotFilled = false;
                }
            }

            if (Physics.Raycast(ray, out hit, 100, damageUpgradeIconMask) && !damageUpgradeTaken)
            {
                Destroy(hit.collider.gameObject);
                damageUpgradeTaken = true;

                if (hit.collider.transform.parent.name == "Turrets/Upgrade_1")
                {
                    us.firstSlotFilled = false;
                }
                else if (hit.collider.transform.parent.name == "Turrets/Upgrade_2")
                {
                    us.secondSlotFilled = false;
                }
                else if (hit.collider.transform.parent.name == "Turrets/Upgrade_3")
                {
                    us.thirdSlotFilled = false;
                }
            }

            if (Physics.Raycast(ray, out hit, 100, fireRateUpgradeIconMask) && !fireRateUpgradeTaken)
            {
                Destroy(hit.collider.gameObject);
                fireRateUpgradeTaken = true;

                if (hit.collider.transform.parent.name == "Turrets/Upgrade_1")
                {
                    us.firstSlotFilled = false;
                }
                else if (hit.collider.transform.parent.name == "Turrets/Upgrade_2")
                {
                    us.secondSlotFilled = false;
                }
                else if (hit.collider.transform.parent.name == "Turrets/Upgrade_3")
                {
                    us.thirdSlotFilled = false;
                }
            }
        }
    }

    void PlaceTurretsOnSupports()
    {
        if (Input.GetMouseButtonUp(0) && !lw.gameFinished)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            // FirstSupport
            if (Physics.Raycast(ray, out hit, 100, firstTurretSupportMask) && basicTurretTaken)
            {
                GameObject turret = Instantiate(basicTurret, hit.transform.position + new Vector3(0, 0.75f, 0), Quaternion.identity);
                basicTurretIsChild = true;
                turret.transform.parent = hit.transform;
                basicTurretTaken = false;
            }
            else if (Physics.Raycast(ray, out hit, 100, firstTurretSupportMask) && rifleTurretTaken)
            {
                GameObject turret = Instantiate(rifleTurret, hit.transform.position + new Vector3(0, 0.75f, 0), Quaternion.identity);
                rifleTurretTaken = false;
                rifleTurretIsChild = true;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 100, firstTurretSupportMask) && areaTurretTaken)
            {
                GameObject turret = Instantiate(areaTurret, hit.transform.position + new Vector3(0, 0.75f, 0), Quaternion.identity);
                areaTurretTaken = false;
                areaTurretIsChild = true;
                turret.transform.parent = hit.transform;
            }

            // SecondSupport
            if (Physics.Raycast(ray, out hit, 100, secondTurretSupportMask) && basicTurretTaken)
            {
                GameObject turret = Instantiate(basicTurret, hit.transform.position + new Vector3(0, 0.75f, 0), Quaternion.identity);
                basicTurretTaken = false;
                basicTurretIsChild = true;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 100, secondTurretSupportMask) && rifleTurretTaken)
            {
                GameObject turret = Instantiate(rifleTurret, hit.transform.position + new Vector3(0, 0.75f, 0), Quaternion.identity);
                rifleTurretTaken = false;
                rifleTurretIsChild = true;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 100, secondTurretSupportMask) && areaTurretTaken)
            {
                GameObject turret = Instantiate(areaTurret, hit.transform.position + new Vector3(0, 0.75f, 0), Quaternion.identity);
                areaTurretTaken = false;
                areaTurretIsChild = true;
                turret.transform.parent = hit.transform;
            }

            // ThirdSupport
            if (Physics.Raycast(ray, out hit, 100, thirdTurretSupportMask) && basicTurretTaken)
            {
                GameObject turret = Instantiate(basicTurret, hit.transform.position + new Vector3(0, 0.75f, 0), Quaternion.identity);
                basicTurretTaken = false;
                basicTurretIsChild = true;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 100, thirdTurretSupportMask) && rifleTurretTaken)
            {
                GameObject turret = Instantiate(rifleTurret, hit.transform.position + new Vector3(0, 0.75f, 0), Quaternion.identity);
                rifleTurretTaken = false;
                rifleTurretIsChild = true;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 100, thirdTurretSupportMask) && areaTurretTaken)
            {
                GameObject turret = Instantiate(areaTurret, hit.transform.position + new Vector3(0, 0.75f, 0), Quaternion.identity);
                areaTurretTaken = false;
                areaTurretIsChild = true;
                turret.transform.parent = hit.transform;
            }
        }
    }

    // Stacking system
    void StackTurret()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            // Basic Turret
            if (Physics.Raycast(ray, out hit, 50, basicTurretMask) && rifleTurretTaken && hit.transform.parent.childCount < 4)
            {                
                GameObject turret = Instantiate(rifleTurret, hit.transform.position + new Vector3(0, 0.55f, 0), Quaternion.identity);
                turret.transform.parent = hit.transform.parent;
                rifleTurretTaken = false;
            }
            else if (Physics.Raycast(ray, out hit, 50, basicTurretMask) && basicTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(basicTurret, hit.transform.position + new Vector3(0, 0.55f, 0), Quaternion.identity);
                basicTurretTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, basicTurretMask) && areaTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(areaTurret, hit.transform.position + new Vector3(0, 0.55f, 0), Quaternion.identity);
                areaTurretTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, basicTurretMask) && rangeUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(rangeUpgrade, hit.transform.position + new Vector3(0, 0.85f, 0), Quaternion.identity);
                rangeUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, basicTurretMask) && damageUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(damageUpgrade, hit.transform.position + new Vector3(0, 0.85f, 0), Quaternion.identity);
                damageUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, basicTurretMask) && fireRateUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(fireRateUpgrade, hit.transform.position + new Vector3(0, 0.85f, 0), Quaternion.identity);
                fireRateUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }

            // Rifle Turret
            if (Physics.Raycast(ray, out hit, 50, rifleTurretMask) && basicTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(basicTurret, hit.transform.position + new Vector3(0, 0.60f, 0), Quaternion.identity);
                basicTurretTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, rifleTurretMask) && rifleTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(rifleTurret, hit.transform.position + new Vector3(0, 0.60f, 0), Quaternion.identity);
                rifleTurretTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, rifleTurretMask) && areaTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(areaTurret, hit.transform.position + new Vector3(0, 0.70f, 0), Quaternion.identity);
                areaTurretTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, rifleTurretMask) && rangeUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(rangeUpgrade, hit.transform.position + new Vector3(0, 0.65f, 0), Quaternion.identity);
                rangeUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, rifleTurretMask) && damageUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(damageUpgrade, hit.transform.position + new Vector3(0, 1f, 0), Quaternion.identity);
                damageUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, rifleTurretMask) && fireRateUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(fireRateUpgrade, hit.transform.position + new Vector3(0, 0.85f, 0), Quaternion.identity);
                fireRateUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }

            // Area Turret
            if (Physics.Raycast(ray, out hit, 50, areaTurretMask) && basicTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(basicTurret, hit.transform.position + new Vector3(0, 0.60f, 0), Quaternion.identity);
                basicTurretTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, areaTurretMask) && rifleTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(rifleTurret, hit.transform.position + new Vector3(0, 0.60f, 0), Quaternion.identity);
                rifleTurretTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, areaTurretMask) && areaTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(areaTurret, hit.transform.position + new Vector3(0, 0.55f, 0), Quaternion.identity);
                areaTurretTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, areaTurretMask) && rangeUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(rangeUpgrade, hit.transform.position + new Vector3(0, 0.80f, 0), Quaternion.identity);
                rangeUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, areaTurretMask) && damageUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(damageUpgrade, hit.transform.position + new Vector3(0, 0.65f, 0), Quaternion.identity);
                damageUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, areaTurretMask) && fireRateUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(fireRateUpgrade, hit.transform.position + new Vector3(0, 0.85f, 0), Quaternion.identity);
                fireRateUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }

            // Range Upgrade
            if (Physics.Raycast(ray, out hit, 50, rangeUpgradeMask) && basicTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(basicTurret, hit.transform.position + new Vector3(0, 1.85f, 0), Quaternion.identity);
                basicTurretTaken = false;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 50, rangeUpgradeMask) && rifleTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(rifleTurret, hit.transform.position + new Vector3(0, 2f, 0), Quaternion.identity);
                rifleTurretTaken = false;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 50, rangeUpgradeMask) && areaTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(areaTurret, hit.transform.position + new Vector3(0, 2f, 0), Quaternion.identity);
                areaTurretTaken = false;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 50, rangeUpgradeMask) && rangeUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(rangeUpgrade, hit.transform.position + new Vector3(0, 2.3f, 0), Quaternion.identity);
                rangeUpgradeTaken = false;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 50, rangeUpgradeMask) && damageUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(damageUpgrade, hit.transform.position + new Vector3(0, 2.3f, 0), Quaternion.identity);
                damageUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, rangeUpgradeMask) && fireRateUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(fireRateUpgrade, hit.transform.position + new Vector3(0, 2.3f, 0), Quaternion.identity);
                fireRateUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }

            // Damage Upgrade
            if (Physics.Raycast(ray, out hit, 50, damageUpgradeMask) && basicTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(basicTurret, hit.transform.position + new Vector3(0, 1.85f, 0), Quaternion.identity);
                basicTurretTaken = false;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 50, damageUpgradeMask) && rifleTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(rifleTurret, hit.transform.position + new Vector3(0, 2f, 0), Quaternion.identity);
                rifleTurretTaken = false;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 50, damageUpgradeMask) && areaTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(areaTurret, hit.transform.position + new Vector3(0, 2f, 0), Quaternion.identity);
                areaTurretTaken = false;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 50, damageUpgradeMask) && rangeUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(rangeUpgrade, hit.transform.position + new Vector3(0, 2.3f, 0), Quaternion.identity);
                rangeUpgradeTaken = false;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 50, damageUpgradeMask) && damageUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(damageUpgrade, hit.transform.position + new Vector3(0, 2.3f, 0), Quaternion.identity);
                damageUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, damageUpgradeMask) && fireRateUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(fireRateUpgrade, hit.transform.position + new Vector3(0, 2.3f, 0), Quaternion.identity);
                fireRateUpgradeTaken = false;
                turret.transform.parent = hit.transform;
            }

            // Fire rate Upgrade
            if (Physics.Raycast(ray, out hit, 50, fireRateUpgradeMask) && basicTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(basicTurret, hit.transform.position + new Vector3(0, 1.85f, 0), Quaternion.identity);
                basicTurretTaken = false;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 50, fireRateUpgradeMask) && rifleTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(rifleTurret, hit.transform.position + new Vector3(0, 2f, 0), Quaternion.identity);
                rifleTurretTaken = false;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 50, fireRateUpgradeMask) && areaTurretTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(areaTurret, hit.transform.position + new Vector3(0, 2f, 0), Quaternion.identity);
                areaTurretTaken = false;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 50, fireRateUpgradeMask) && rangeUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(rangeUpgrade, hit.transform.position + new Vector3(0, 2.3f, 0), Quaternion.identity);
                rangeUpgradeTaken = false;
                turret.transform.parent = hit.transform;
            }
            else if (Physics.Raycast(ray, out hit, 50, fireRateUpgradeMask) && damageUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(damageUpgrade, hit.transform.position + new Vector3(0, 2.3f, 0), Quaternion.identity);
                damageUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
            else if (Physics.Raycast(ray, out hit, 50, fireRateUpgradeMask) && fireRateUpgradeTaken && hit.transform.parent.childCount < 4)
            {
                GameObject turret = Instantiate(damageUpgrade, hit.transform.position + new Vector3(0, 2.3f, 0), Quaternion.identity);
                damageUpgradeTaken = false;
                turret.transform.parent = hit.transform.parent;
            }
        }
    }
}
