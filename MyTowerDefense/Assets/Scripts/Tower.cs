using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tower : MonoBehaviour
{
    LevelWaves lw;

    int maxHP = 200;
    [SerializeField] float currenHP;

    [SerializeField] GameObject bar;
    [SerializeField] Image health;

    private void Start()
    {
        lw = FindObjectOfType<LevelWaves>();

        currenHP = maxHP;
    }

    private void Update()
    {
        if(currenHP <= 0)
        {
            lw.gameFinished = true;
        }

        health.fillAmount = currenHP / 200;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "StandardEnemy")
        {
            currenHP -= 10;
        }
        if (other.tag == "FastEnemy")
        {
            currenHP -= 5;
        }
        if (other.tag == "TankEnemy")
        {
            currenHP -= 20;
        }
    }
}
